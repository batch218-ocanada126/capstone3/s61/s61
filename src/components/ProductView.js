import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

import ProductQuantity from '../components/ProductQuantity';

import Swal from 'sweetalert2';

import { useParams, useNavigate, Link } from 'react-router-dom';


export default function ProductView() {

	//declaration that it will globally
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// 'useParams' - hook allows us to retrieve the productId passed via the URL params
	const {productId} = useParams();
	
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [totalAmount, setTotalAmount] = useState(0);


	const handleQuantityChange = (newQuantity) => {
	setQuantity(newQuantity);
	setTotalAmount(price * newQuantity);

		}



	const buyNow = (productId) => {
	    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
	      method: "POST",
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	      },
	      body: JSON.stringify({
	        productId: productId,
	        quantity: quantity,
	       totalAmount: totalAmount
	      })
	    })
	      .then(res => res.json())
	      .then(data => {
	        console.log(data)

	        if (data === true) {
	          Swal.fire({
	            title: "Successfully Checkout",
	            icon: "success",
	            text: "You have successfully order for this product."
	          })

	          navigate("/products");

	        } else {
	          Swal.fire({
	            title: "Something went wrong",
	            icon: "error",
	            text: "Please try again."
	          })
	        }
	      })
	  }



	 


	


	useEffect(() =>{

		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setTitle(data.title);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
			setTotalAmount(data.totalAmount);
		})

			
	}, [productId])


	return (

		<div className="container-fluid " id="bg-productView">
			
				<Card className=" min-vh-100">
							
					      <Card.Body className="text-center row ">

					      		<div className="col-md-12 col-lg-6  p-5">
					        		<Card.Title>{title}</Card.Title>
					       			
					       		</div>	

					       		<div className="col-md-12 col-lg-6 p-5">	
					        		<Card.Subtitle className="">Description:</Card.Subtitle>
					        		        <Card.Text>{description}</Card.Text>
					        		        <Card.Text>Price: ${price}</Card.Text>
					        		        <ProductQuantity 
					        		          handleQuantityChange={handleQuantityChange}
					        		          quantity={quantity}
					        		          setTotalAmount={setTotalAmount}
					        		        />
					        		        <Card.Text className="m-3">Total Amount: ${totalAmount}</Card.Text>

					        		        {
					        		        (user.id !== null) ?
					        		        <div>
					        		        	<Button onClick={() => buyNow(productId)}>Buy Now</Button>
					        		        	<Link to="/products">
					        		       		<Button className="m-2 btn-yellow">Back to products</Button>
					        		       		</Link>
					        		       	</div>
					        		        :
					        				<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Buy</Button>   		 
					        		        }   		 
					        	</div>


					       		{/*<div className="col-lg-12 offset-3 p-5">
					       			<Card.Subtitle>Price:</Card.Subtitle>
					        		<Card.Text>PhP {price}</Card.Text>
					        	</div>
					        	<div className="container fluid  ">
					        		<div className="row ">
					       			 <Card.Text className="col-3">Quantity:</Card.Text>
					       			<ProductQuantity 
					       			 initialQuantity={1} 
					       			 productId={productId} 
					       			 price={price}
					       			
					       			 
					       			 setTotalAmount={setTotalAmount}
					       			/>
					        		
					        		 <Card.Text className="col-8">Total Amount:</Card.Text>
					        		
					        		
					        		</div> 
					        	</div>
					        		*/}
					       		

					        
					        *{/*{
					        	(user.id !== null) ? <div id="bg-buyNow" className="col-12 d-flex justify-content-center ">
					       			
					        		<Button variant="border border-primary m-2" onClick={() => buyNow(productId)}	><img classImage="m-3 rounded"width="80" src="../images/cartlogo.gif"></img></Button>
					        		<Button variant="primary" onClick={() => buyNow(productId)} >Buy Now</Button>
					        		</div>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Buy</Button>
					        	
					        }*/}

					      </Card.Body>
					</Card>
			
			
		</div>

		)
	
}